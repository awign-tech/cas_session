# frozen_string_literal: true

require 'warden'
require 'connection_pool'
require_relative 'cas_session/strategies/cas_headers'
require_relative 'cas_session/strategies/cas_cache'
require_relative 'cas_session/strategies/cas_api'
# require_relative 'cas_session/strategies/cas_db'
module CasSession # :nodoc:
  concurrency = (ENV.fetch('WEB_CONCURRENCY') { 2 }) * 2
  cache_host = ENV.fetch('CAS_SESSION_REDIS_HOST') { 'localhost:6379/10' }
  SessionCache = ConnectionPool.new(size: concurrency,
                                    timeout: 10) { Redis.new(url: "redis://#{cache_host}") }

  ::Warden::Strategies.add(:cas_headers, Strategies::CasHeaders)
  ::Warden::Strategies.add(:cas_cache, Strategies::CasCache)
  ::Warden::Strategies.add(:cas_api, Strategies::CasApi)
  # ::Warden::Strategies.add(:cas_headers, Strategies::CasDb)
end
