# frozen_string_literal: true

module CasSession
  module Authentication
    module Default # :nodoc:
      module Skip # :nodoc:
        private

        # no-op
        def authenticate!; end
      end

      def self.included(action)
        action.class_eval do
          if defined? Hanami
            before :authenticate!
          elsif defined? Rails
            before_action :authenticate!
          end
          attr_reader :current_user
        end
      end

      def initialize(authenticator: nil, **args)
        @_authenticator = authenticator

        if args.any?
          super(**args)
        else
          super()
        end
      end

      private

      def authenticate!
        if ENV['STRICT_AUTH_ENABLED'].eql? 'true'
          authenticator.authenticate!
          Thread.current[:current_user] = authenticator.user.symbolize_keys
        else
          access_token = request.env['HTTP_ACCESS_TOKEN'] || request.env['HTTP-ACCESS-TOKEN']
          return nil unless access_token.present?

          authenticator.authenticate!
          Thread.current[:current_user] = authenticator.user.symbolize_keys
        end
      end

      def authenticator
        @_authenticator || request.env['warden']
      end
    end
  end
end
