# frozen_string_literal: true

module CasSession
  module Strategies
    class CasHeaders < ::Warden::Strategies::Base #:nodoc:
      HEADERS = %w[HTTP_UID HTTP_CLIENT
                   HTTP_ACCESS_TOKEN].freeze

      def authenticate!
        return true unless current_user.nil?

        fail!(failure_message) if missing_headers.any?
      end

      def store?
        false
      end

      private

      def headers
        request.env
               .slice(*HEADERS)
               .transform_keys do |key|
                 key.gsub(/HTTP_/, '')
                    .downcase.to_sym
               end
      end

      def missing_headers
        @_missing = []
        HEADERS.each do |header|
          next unless request.env[header].nil?

          message = I18n.translate('cas_session.errors.headers.missing' \
                                              ".#{header}")
          @_missing << message
        end
        @_missing
      end

      def failure_message
        @_missing.first
      end

      def current_user
        request.env['warden'].user
      end
    end
  end
end
