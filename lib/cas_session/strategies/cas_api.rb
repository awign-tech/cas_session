# frozen_string_literal: true

module CasSession
  module Strategies
    class CasApi < ::Warden::Strategies::Base #:nodoc:
      BASE_URL = ENV.fetch('CAS_URL') { 'http://localhost:2301' }
      VALIDATE_ROUTE = '/central/auth/validate'
      HEADERS = %w[HTTP_UID HTTP_CLIENT
                   HTTP_ACCESS_TOKEN
                   HTTP_X_CLIENT_ID].freeze

      def authenticate!
        return true unless current_user.nil?

        validate!
      end

      def store?
        false
      end

      private

      def validate!
        if validate_api_response.code.eql? '200'
          data = JSON.parse(validate_api_response.body)['data']['user']
          success!(data)
        else
          fail!('Invalid headers')
        end
      end

      def headers
        request.env
               .slice(*HEADERS)
               .transform_keys do |key|
          key.gsub(/HTTP_/, '')
             .downcase.to_sym
        end
      end

      def current_user
        request.env['warden'].user
      end

      def validate_api_response
        uri = URI.parse(BASE_URL + VALIDATE_ROUTE)
        https = Net::HTTP.new(uri.host, uri.port)
        https.use_ssl = true
        request = Net::HTTP::Get.new(uri)
        request['access-token'] = headers[:access_token]
        request['client'] = headers[:client]
        request['uid'] = headers[:uid]
        request['x-client-id'] = headers[:x_client_id]
        https.request(request)
      end
    end
  end
end