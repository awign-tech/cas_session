# frozen_string_literal: true

module CasSession
  module Strategies
    class CasCache < ::Warden::Strategies::Base #:nodoc:
      CLIENT = 'HTTP_CLIENT'
      UID = 'HTTP_UID'

      def authenticate!
        return true unless current_user.nil?

        validate!
      end

      def store?
        false
      end

      private

      def validate!
        data = SessionCache.with do |redis|
          cas_session_redis_key = request.env[UID] + '_' + request.env[CLIENT]
          redis.get(cas_session_redis_key)
        end
        return false if data.nil?

        user = JSON.parse(data)
        success!(user)
      end

      def current_user
        request.env['warden'].user
      end
    end
  end
end
