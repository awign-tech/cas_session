# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'cas_session'
  s.version     = '0.1.7'
  s.date        = '2020-01-15'
  s.summary     = 'Authentication mechanism for CAS'
  s.description = 'This uses CAS to do authentication for microservices in awign'
  s.authors     = ['Manav Sharma']
  s.files       = Dir['{config,lib}/**/*']
  s.require_paths = ['lib']

  s.homepage    = 'https://www.awign.com'
  s.email       = ['manav.sharma@awign.com']
  s.license     = 'Nonstandard'

  s.add_dependency 'connection_pool'
  s.add_dependency 'http'
  s.add_dependency 'redis'
  s.add_dependency 'warden'
end
